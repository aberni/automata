Pour compiler en ligne de commande :
javac -d bin -cp bin -sourcepath automata automata/*.java

Pour executer :
java -cp bin automata.Test

Pour executer les tests JUnit sous IntelliJ, ajouter les librairies suivantes au projet :
"junit.jar", "junit-X.X.jar" et "hamcrest-core-X.X"
Elles peuvent etre trouvees dans <Repertoire IntelliJ IDEA>\lib.

package automata;

import java.util.Comparator;

public class EA {
    public static final int SELECTION_SIZE = 2;
    public static final int NB_PARENTS_CROSSOVER = 2;
    private static final Comparator<Solution> COMPARATOR_REPLACEMENT = Comparator.comparingInt(Solution::getFitness);

    /**
     * Initialise la population aléatoirement
     * @param population
     */
    public void init(Population population) {
        for(Solution sol : population) {
            Initialization.init(sol);
        }
    }

    /**
     * Selectionne aléatoirement des elements de la population
     * pour les mettre dans la listes de geniteurs
     * @param population
     * @param geniteurs
     */
    public void select(Population population, Population geniteurs) {
        population.sort(COMPARATOR_REPLACEMENT);
        geniteurs.addAll(population.subList(population.size()-SELECTION_SIZE, population.size()));
    }

    /**
     * Methode de variation de la population, crossover ou permutation, etc
     * @param population
     */
    public void variation(Population population) {
        crossover(population);
    }

    /**
     * Methode de crossover aleatoire parmi les membres de la population
     * @param population
     */
    public void crossover(Population population) {
        int frs = Initialization.freeRules.size();
        int midIndex = frs/2, index;
        int tmp;
        for(int i=1; i<population.size(); i+=2) {
            for(int j=midIndex; j<frs; j++) {
                index = Initialization.freeRules.get(j);
                tmp = population.get(i).getRuleAt(index);
                population.get(i).setRuleAt(index, population.get(i-1).getRuleAt(index));
                population.get(i-1).setRuleAt(index, tmp);
            }
        }
    }

    public void remplacement(Population parents, Population geniteurs) {
        int oldSize = parents.size();
        parents.addAll(geniteurs);
        parents.sort(COMPARATOR_REPLACEMENT);
        while(parents.size() > oldSize) {
            parents.removeElementAt(0);
        }
    }
}

package automata;

import java.util.Comparator;

public class HillClimber {
    private Automata automata;
    private Initialization initialization;
    private Solution solution, bestSolution;
    private static final Comparator<Solution> COMPARATOR_SOLUTION = Comparator.comparingInt(Solution::getFitness);

    public HillClimber(Automata automata) {
        this.automata = automata;
        initialization = new Initialization();
    }

    public Solution getSolution(Solution solution, int nTests) {
        Solution neighbor;
        bestSolution = new Solution(solution);
        NeighborGenerator generator = new NeighborGenerator(bestSolution);

        for(int i=0; i<nTests; i++) {
            neighbor = generator.getRandomNeighbor();
            automata.eval(neighbor, automata.maxSize);
            if(neighbor.getFitness() >= bestSolution.getFitness()) {
                bestSolution.copySolution(neighbor);
            }
        }
        return bestSolution;
    }

    public Solution getSolution(Solution solution) {
        Solution neighbor = new Solution(solution);
        bestSolution = new Solution(solution);
        NeighborGenerator generator = new NeighborGenerator(neighbor);

        while(generator.hasNext()) {
            neighbor = generator.nextNeighbor();
            automata.eval(neighbor, automata.maxSize);
            if(neighbor.getFitness() >= bestSolution.getFitness()) {
                bestSolution.copySolution(neighbor);
            }
        }
        return bestSolution;
    }
}
package automata;

import java.util.Collections;
import java.util.Random;
import java.util.Vector;

/**
 * @author verel
 *
 */
public class Initialization {

    public static final int nbRules = 216;
    public static final Vector<Integer> freeRules = new Vector<>();
    private static final Initialization singleton = new Initialization();

    public static final Random generator = new Random();

    public Initialization() {
        if(singleton == null)
            saveIndexesOfFreeRules();
    }

    public void saveIndexesOfFreeRules() {
        for(int i=0; i<4; i++) {
            for(int j=0; j<4; j++) {
                for(int k=0; k<4; k++) {
                    if( ! (i==j && j==k && (k==0 || k==1)) && (i!=1 && i!=2 || j!=0 || k!=0 ) )
                        freeRules.add(toIndex(i,j,k));
                }
            }
        }
        for(int i=0; i<4; i++) {
            for(int j=0; j<4; j++) {
                if(!(j==0 && (i==0 || i==1)) && !(i==1 && j==1))
                    freeRules.add(toIndex(5, i,j));
            }
        }
        for(int i=0; i<4; i++) {
            for(int j=0; j<4; j++) {
                if(!(j==0 && (i==0 || i==1 || i==2)) && !(i==1 && j==1))
                    freeRules.add(toIndex(i,j, 5));
            }
        }
        Collections.sort(freeRules);
    }

    public void init(int [] rules) {
        // les regles repos (obligatoires)
        setRule(rules, 0, 0, 0, 0);
        setRule(rules, 5, 0, 0, 0);
        setRule(rules, 0, 0, 5, 0);

        // les regles feu (trés conseillés)
        setRule(rules, 1, 1, 1, 4);
        setRule(rules, 5, 1, 1, 4);
        setRule(rules, 1, 1, 5, 4);

        // les regles a priori (signal de période 2 vers la droite)
        setRule(rules, 1, 0, 0, 2);
        setRule(rules, 2, 0, 0, 1);

        // a priori bord droit
        setRule(rules, 1, 0, 5, 1);
        setRule(rules, 2, 0, 5, 2);

        // a priori bord gauche
        setRule(rules, 5, 1, 0, 1);
    }

    public static void init(Solution solution) {
        solution.setRuleAt(toIndex(0, 0, 0), 0);
        solution.setRuleAt(toIndex(5, 0, 0), 0);
        solution.setRuleAt(toIndex(0, 0, 5), 0);

        solution.setRuleAt(toIndex(1, 1, 1), 4);
        solution.setRuleAt(toIndex(5, 1, 1), 4);
        solution.setRuleAt(toIndex(1, 1, 5), 4);

        solution.setRuleAt(toIndex(1,0,0), 2);
        solution.setRuleAt(toIndex(2,0,0), 1);

        solution.setRuleAt(toIndex(1,0,5), 1);
        solution.setRuleAt(toIndex(2,0,5), 2);

        solution.setRuleAt(toIndex(5,1,0), 1);
    }

    /*
     * Ecrit la regle
     *
     * g : etat de la cellule de gauche
     * c : etat de la cellule centrale
     * d : etat de la cellule de droite
     * r : etat de la cellule centale à t+1
     */
    public void setRule(int [] rules, int g, int c, int d, int r) {
        rules[toIndex(g,c,d)] = r;
    }

    private static int toIndex(int g, int c, int d) {
        return g * 36 + c * 6 + d;
    }

    public void initFreeRulesRandomly(Solution sol) {
        for(Integer i : freeRules) {
            sol.setRuleAt(i, generator.nextInt(4));
        }
    }

    public static int getRandomIndexOfFreeRules() {
        return generator.nextInt(freeRules.size());
    }
}

package automata;

public class IteratedLocalSearch {
    private Automata automata;
    private int nTests, nHillClimber, nPerturbations;
    private HillClimber hillClimber;
    private Solution solution;

    public IteratedLocalSearch(Automata automata, int nTests, int nHillClimber, int nPertubations) {
        this.automata = automata;
        this.nTests = nTests;
        this.nHillClimber = nHillClimber;
        this.nPerturbations = nPertubations;
        hillClimber = new HillClimber(automata);
        solution = new Solution(Initialization.nbRules);
        Initialization.init(solution);
        automata.eval(solution, automata.maxSize);
    }

    public void setSolution(Solution solution) {
        this.solution = new Solution(solution);
    }

    public Solution getSolution() {
        solution = hillClimber.getSolution(solution,nHillClimber);

        Solution bestSolution = new Solution(solution);
        NeighborGenerator generator = new NeighborGenerator(solution);

        int previous = 0;
        System.out.println(previous+"% fitness: "+bestSolution.getFitness());
        for(int i=0; i<nTests; i++) {
            if(100*i/nTests != previous) {
                previous = 100*i/nTests;
                System.out.println(previous+"% fitness: "+bestSolution.getFitness());
            }
            for(int j=0; j<nPerturbations; j++) {
                solution = generator.getRandomNeighbor();
            }
            automata.eval(solution, automata.maxSize);

            solution = hillClimber.getSolution(solution, nHillClimber);

            if(solution.getFitness() > bestSolution.getFitness()) {
                bestSolution.copySolution(solution);
            } else {
                solution.copySolution(bestSolution);
            }
        }
        return bestSolution;
    }
}

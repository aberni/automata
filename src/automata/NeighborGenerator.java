package automata;

public class NeighborGenerator {
    private Solution sol;
    private int index, i;

    public NeighborGenerator(Solution solution) {
        setSolution(solution);
    }

    public void setSolution(Solution solution) {
        sol = solution;
        index = 0;
        i = 1;
    }

    public boolean hasNext() {
        return index < Initialization.freeRules.size();
    }

    public Solution nextNeighbor() {
        Solution neighbor = new Solution(sol);
        neighbor.setRuleAt(
                Initialization.freeRules.get(index),
                (neighbor.getRuleAt(Initialization.freeRules.get(index))+i++)%4
        );
        if(i > 3) {
            index++;
            i = 1;
        }
        return neighbor;
    }

    public Solution getRandomNeighbor() {
        Solution neighbor = new Solution(sol);
        neighbor.setRuleAt(
                Initialization.freeRules.get(Initialization.generator.nextInt(Initialization.freeRules.size())),
                Initialization.generator.nextInt(4)
        );
        return neighbor;
    }
}

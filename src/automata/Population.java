package automata;

import java.util.Vector;

public class Population extends Vector<Solution> {
    public Population(Vector<Solution> population) {
        addAll(population);
    }

    public Population(Solution ... solutions) {
        for(Solution sol : solutions)
            add(sol);
    }

    public Population(Population population) {
        for(Solution sol : population) {
            add(new Solution(sol));
        }
    }

    public Population(int size) {
        while(size-- > 0) {
            add(new Solution(Initialization.nbRules));
        }
    }
}

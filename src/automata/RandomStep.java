package automata;

import java.util.Comparator;

public class RandomStep {
    private Automata automata;
    private Initialization initialization;
    private Solution solution, bestSolution;
    private static final Comparator<Solution> COMPARATOR_SOLUTION = Comparator.comparingInt(Solution::getFitness);

    public RandomStep(Automata automata) {
        this.automata = automata;
        initialization = new Initialization();
    }

    public Solution getSolution(int nTests) {
        solution = new Solution(Initialization.nbRules);
        bestSolution = new Solution(Initialization.nbRules, -1);

        Initialization.init(solution);

        for(int i=0; i<nTests; i++) {
            initialization.initFreeRulesRandomly(solution);
            automata.eval(solution, automata.maxSize);
            if(solution.getFitness() > bestSolution.getFitness()) {
                bestSolution.copySolution(solution);
            }
        }
        return bestSolution;
    }

    public Population getSolutions(int size, int nTests) {
        Population population = new Population();

        for(int i=0; i<nTests; i++) {
            solution = new Solution(Initialization.nbRules);
            initialization.init(solution);
            initialization.initFreeRulesRandomly(solution);
            automata.eval(solution, automata.maxSize);
            population.add(solution);
            if(population.size() > size) {
                population.sort(COMPARATOR_SOLUTION);
                population.removeElementAt(0);
            }
        }
        return population;
    }
}

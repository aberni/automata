package automata;

import java.util.Arrays;

public class Solution {
    private int [] rules;
    private int fitness;

    public Solution(int nbRules) {
        rules = new int[nbRules];
    }

    public Solution(int nbRules, int fitness) {
        this(nbRules);
        this.fitness = fitness;
    }

    public Solution(Solution solution) {
        rules = new int[solution.rules.length];
        copySolution(solution);
    }

    public Solution(String parseString) {
        String[] numbersStr = parseString.split(" ");
        rules = new int[numbersStr.length];
        fitness = Integer.valueOf(numbersStr[0]);
        for(int i=1; i<numbersStr.length; i++) {
            rules[i-1] = Integer.valueOf(numbersStr[i]);
        }
    }

    public void copySolution(Solution solution) {
        System.arraycopy(solution.rules, 0, rules, 0, solution.rules.length);
        fitness = solution.fitness;
    }

    public Solution neighbor() {
        Solution neighbor = new Solution(this);
        int index = Initialization.getRandomIndexOfFreeRules();
        neighbor.setRuleAt(index, (neighbor.getRuleAt(index)+1)%4);

        return neighbor;
    }

    public int[] getRules() {
        return rules;
    }

    public int getFitness() {
        return fitness;
    }

    public int getRuleAt(Integer i) {
        return rules[i];
    }

    public void setRuleAt(Integer i, int value) {
        rules[i] = value;
    }

    public void setFitness(int fitness) {
        this.fitness =  fitness;
    }

    @Override
    public String toString() {
        return "Solution{" +
                "fitness=" + fitness +
                ", rules=" + Arrays.toString(rules) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Solution) {
            Solution sol = (Solution)o;
            return sol.getFitness() == getFitness() && Arrays.equals(sol.getRules(), getRules());
        }
        return super.equals(o);
    }

    public String freeRulesString() {
        int[] freeRules = new int[Initialization.freeRules.size()];
        for(int i = 0; i<Initialization.freeRules.size(); i++) {
            freeRules[i] = getRuleAt(Initialization.freeRules.get(i));
        }
        return "Solution{" +
                "freeRules=" + Arrays.toString(freeRules) +
                '}';
    }
}

package automata;

import java.util.Vector;
import java.util.Comparator;

public class SolutionAnalyzer {
    private Population population;
    private Automata automata;
    private IteratedLocalSearch iteratedLocalSearch;
    public SolutionAnalyzer() {
        automata = new Automata(20);
        population = new Population();
        iteratedLocalSearch = new IteratedLocalSearch(automata, 3, 100000, 2);
    }

    public void generatePopulation(int sizePopulation) {
        population.removeAllElements();
        for(int i=0; i<sizePopulation; i++) {
            //System.out.println("GENERATING PEOPLE "+(i+1)+"...");
            population.add(iteratedLocalSearch.getSolution());
        }
    }

    public void generatePopulation(int sizePopulation, String parseStr) {
        iteratedLocalSearch.setSolution(new Solution(parseStr));
        generatePopulation(sizePopulation);
    }

    public void findCommonRules(Vector<Integer> indexes, Vector<Integer> rules) {
        boolean b;
        indexes.removeAllElements();
        rules.removeAllElements();
        
        for(int i=0, index, j; i<Initialization.freeRules.size(); i++) {
            b = true;
            index = Initialization.freeRules.get(i);
            for(j=1; j<population.size() && b; j++) {
                b = population.get(0).getRuleAt(index) == population.get(j).getRuleAt(index);
            }
            if(b) {
                rules.add(population.get(0).getRuleAt(index));
                indexes.add(index);
            }
        }
    }

    public void analyzePopulation() {
        generatePopulation(10);
        population.sort(Comparator.comparingInt(Solution::getFitness));
        int lowerFitness = population.firstElement().getFitness();
        Vector<Integer> commonFreeRules = new Vector<>(),
        indexesCommonFreeRules = new Vector<>();

        findCommonRules(indexesCommonFreeRules, commonFreeRules);
        Test.compareWithOldFitnessAndSave(population.lastElement(), automata);

        System.out.println();
        System.out.println("FINISHED");
        System.out.println("Min fitness: "+lowerFitness);
        System.out.println("Max fitness: "+population.lastElement().getFitness());

        Solution mutant = generateMutant(indexesCommonFreeRules, commonFreeRules);
        automata.eval(mutant, automata.maxSize);

        showMutantRules(indexesCommonFreeRules, commonFreeRules);

        System.out.println("Fitness de base: "+mutant.getFitness());
    }

    public void generateMutantPopulation(int sizePopulation, Vector<Integer> indexes, Vector<Integer> rules) {
        Solution mutant = generateMutant(indexes, rules);
        iteratedLocalSearch.setSolution(mutant);
        generatePopulation(sizePopulation);
    }

    /**
     *
     * @param nGenerations number of generations (better if > 2)
     */
    public void notDumbSearch(int nGenerations, int sizeOfPopulations) {
        Vector<Integer> commonFreeRules = new Vector<>(),
                indexesCommonFreeRules = new Vector<>();
        Vector<Integer> mutantRules = new Vector<>(),
                mutantIndexes = new Vector<>();
        int fitnessMutant = 2;
        Solution bestMutant = new Solution(Initialization.nbRules),
         bestSol = new Solution(Initialization.nbRules);

        for(int i=0; i<nGenerations; i++) {
            System.out.println("### GENERATION N°"+(i+1)+" ###");
            // generation d'une population (la prémière est saine)
            generateMutantPopulation(sizeOfPopulations, mutantIndexes, mutantRules);

            // trouver les similitudes pour générer un mutant à analyser
            findCommonRules(indexesCommonFreeRules, commonFreeRules);

            // generation du mutant
            Solution mutant = generateMutant(indexesCommonFreeRules, commonFreeRules);
            automata.eval(mutant, automata.maxSize);

            if(fitnessMutant < mutant.getFitness()) {
                fitnessMutant = mutant.getFitness();
                mutantIndexes = new Vector<>(indexesCommonFreeRules);
                mutantRules = new Vector<>(commonFreeRules);
                bestMutant.copySolution(population.lastElement());
            } else if(fitnessMutant == mutant.getFitness()){
                if(population.lastElement().getFitness() > bestMutant.getFitness()) {
                    bestMutant.copySolution(population.lastElement());
                }
            }
            if(population.lastElement().getFitness() > bestMutant.getFitness()) {
                bestMutant.copySolution(population.lastElement());
            }
        }

        Test.compareWithOldFitnessAndSave(bestMutant, automata);
        System.out.println();
        showMutantRules(mutantIndexes, mutantRules);
        System.out.println("Fitness des mutants: "+fitnessMutant);
        System.out.println("Best mutant fitness: "+bestMutant.getFitness());
    }

    public Solution generateMutant(Vector<Integer> indexes, Vector<Integer> rules) {
        Solution mutant = new Solution(Initialization.nbRules);
        Initialization.init(mutant);
        for(int i=0; i<indexes.size(); i++) {
            mutant.setRuleAt(indexes.get(i), rules.get(i));
        }
        return mutant;
    }

    public void showMutantRules(Vector<Integer> indexes, Vector<Integer> rules) {
        for(int i=0; i<indexes.size(); i++) {
            System.out.println("solution.setRuleAt("+indexes.get(i)+", "+rules.get(i)+");");
        }
    }

    public static void main(String ...args) {
        SolutionAnalyzer solutionAnalyzer = new SolutionAnalyzer();
        //solutionAnalyzer.analyzePopulation();
        solutionAnalyzer.notDumbSearch(50,20);
    }
}

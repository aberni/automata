package automata;

import java.io.*;
import java.nio.file.Paths;
import java.util.Scanner;


/**
 * @author verel
 *
 */
public class Test {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // Nombre maximale de fusiliers (taille maximale du réseau)
        int sizeMax = 20;

        Automata automata = new Automata(sizeMax);
        Solution bestSol = new Solution(Initialization.nbRules);
        Initialization.init(bestSol);

        System.out.println("ITEREATED LOCAL SEARCH");
        IteratedLocalSearch iteratedLS = new IteratedLocalSearch(automata, 1000, 100000, 2);

        iteratedLS.setSolution(bestSol);
        bestSol = iteratedLS.getSolution();


        System.out.println(bestSol);
        System.out.println(bestSol.freeRulesString());
        System.out.println();

        compareWithOldFitnessAndSave(bestSol, automata);

        System.out.println("Nouvelle sol: "+bestSol.freeRulesString());
        System.out.println("Fitness: "+bestSol.getFitness());
        System.out.println("The End.");
    }

    public static void compareWithOldFitnessAndSave(Solution bestSol, Automata automata) {
        String path = findPath() + "svg/" + automata.maxSize;
        String filePath = path +"/" + bestSol.getFitness();
        String defaultFileName = "sol_"+automata.maxSize+"_";
        String fileName = defaultFileName + bestSol.getFitness();

        File svgFile = new File(path);
        int oldFit = getOldFitness(svgFile.getAbsolutePath(), defaultFileName);

        if(oldFit < bestSol.getFitness()) {
            File dir = new File(filePath);
            dir.mkdirs();
            System.out.println("Sauvegarde du résultat (old max fitness: "+oldFit+")...");
            saveBestResults(dir.getAbsolutePath(), fileName, automata, bestSol);
        } else System.out.println("Ancien fitness meilleur: "+oldFit);
    }

    private static int getOldFitness(String dirPath, String defaultFileName) {
        try {
            int fitness = 0;
            File file = new File(dirPath);
            String[] directories = file.list((current, name) -> new File(current, name).isDirectory() && isNumeric(name));
            if(directories != null && directories.length > 0) {
                int i;
                for(String fitnessStr : directories) {
                    i = Integer.valueOf(fitnessStr);
                    if(fitness < i)
                        fitness = i;
                }
                dirPath += "/"+fitness+"/";
            } else {
                System.out.println("Aucune précédente solution n'a été trouvée.");
                return -1;
            }

            Scanner scanner = new Scanner(new File(dirPath+defaultFileName+fitness+".dat"));
            if(scanner.hasNextInt()){
                return scanner.nextInt();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.err.println("Vérifiez que le dossier "+dirPath+" n'est pas vide.");
        }
        return -1;
    }

    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
        } catch(NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    private static void saveBestResults(String filePath, String fileName, Automata automate, Solution solution) {
        PrintWriter ecrivain;
        try {
            ecrivain =  new PrintWriter(new BufferedWriter(new FileWriter(filePath + "/" + fileName + ".dat")));

            printToFile(solution.getFitness(), solution.getRules(), ecrivain);

            ecrivain.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        int nFire;
        for(int i = 2; i <= automate.maxSize; i++) {
            // évolution de l'automate avec la règle rule sur un réseau de longueur i
            nFire = automate.evol(solution.getRules(), i);

            // affichage du nombre de fusiliers ayant tiré
            System.out.println("longueur " + i + " : " + nFire);

            // affiche la dynamique dans un fichier au format svg
            automate.exportSVG(i, 2 * i - 2, filePath + "/" + fileName + "_" + i + ".svg");
        }
    }

    private static String findPath() {
        try {
            return new File("").getCanonicalPath() + "/";
        } catch (IOException e) {
            e.printStackTrace();
            return Paths.get("").toString();
        }
    }

    public static void printToFile(int fitness, int [] rules, PrintWriter ecrivain) {
        ecrivain.print(fitness);
        for(int i = 0; i < Initialization.nbRules; i++) {
            ecrivain.print(" ");
            ecrivain.print(rules[i]);
        }
        ecrivain.println();
    }

    public static int [] initRulesFromFile(String fileName) {
        // 5 états + l'état "bord"
        int n = 5 + 1;

        int [] rules = new int[n * n * n];

        try {
            FileReader fichier = new FileReader(fileName);

            StreamTokenizer entree = new StreamTokenizer(fichier);

            int i = 0;
            while(entree.nextToken() == StreamTokenizer.TT_NUMBER) {
                rules[i] = (int) entree.nval;
                i++;
            }
            fichier.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rules;
    }
}

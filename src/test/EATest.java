package test;

import automata.EA;
import automata.Initialization;
import automata.Population;
import automata.Solution;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EATest {
    private static Population pop;
    private static EA ea = new EA();

    @Before
    public void setUp() throws Exception {
        pop = new Population(
                new Solution(Initialization.nbRules),
                new Solution(Initialization.nbRules),
                new Solution(Initialization.nbRules),
                new Solution(Initialization.nbRules),
                new Solution(Initialization.nbRules),
                new Solution(Initialization.nbRules)
        );

        ea.init(pop);
    }

    @Test
    public void init() {
        for(Solution solution : pop) {
            assertEquals(0, solution.getRuleAt(0));
            assertEquals(0, solution.getRuleAt(5*36));
            assertEquals(0, solution.getRuleAt(5));

            assertEquals(4, solution.getRuleAt(36+6+1));
            assertEquals(4, solution.getRuleAt(5*36+6+1));
            assertEquals(4, solution.getRuleAt(36+6+5));

            assertEquals(1, solution.getRuleAt(5*36+6));
            assertEquals(1, solution.getRuleAt(36+5));
        }
    }

    @Test
    public void select() {
        Population geniteurs = new Population();
        assertEquals(0, geniteurs.size());

        ea.select(pop, geniteurs);

        assertEquals(EA.SELECTION_SIZE, geniteurs.size());
    }

    @Test
    public void variation() {
    }

    @Test
    public void crossover() {
        Solution sol1 = new Solution(Initialization.nbRules);
        Solution sol2 = new Solution(Initialization.nbRules);

        for(int i=0; i<Initialization.freeRules.size(); i++) {
            sol1.setRuleAt(Initialization.freeRules.get(i), 1);
        }

        Population population = new Population(sol1, sol2);

        assertEquals("Solution{freeRules=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]}",
                sol1.freeRulesString());
        assertEquals("Solution{freeRules=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]}",
                sol2.freeRulesString());

        ea.crossover(population);

        assertEquals("Solution{freeRules=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]}",
                sol1.freeRulesString());
        assertEquals("Solution{freeRules=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]}",
                sol2.freeRulesString());
    }

    @Test
    public void remplacement() {
        int[] fitnesses = {4,7,6,2,9,1};
        for(int i=0; i<pop.size(); i++) {
            pop.get(i).setFitness(fitnesses[i]);
        }

        StringBuilder stringBuilder = new StringBuilder();
        for(Solution sol : pop) {
            stringBuilder.append(sol.getFitness());
        }
        assertEquals("476291", stringBuilder.toString());

        Population geniteurs = new Population(
                new Solution(Initialization.nbRules, 5),
                new Solution(Initialization.nbRules, 8),
                new Solution(Initialization.nbRules, 3)
        );

        ea.remplacement(pop, geniteurs);

        stringBuilder = new StringBuilder();
        for(Solution sol : pop) {
            stringBuilder.append(sol.getFitness());
        }

        assertEquals("456789", stringBuilder.toString());
    }
}
package test;

import automata.Initialization;
import automata.NeighborGenerator;
import automata.Population;
import automata.Solution;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class NeighborGeneratorTest {

    @Test
    public void testGeneration() {
        Solution solution = new Solution(Initialization.nbRules);
        NeighborGenerator generator = new NeighborGenerator(solution);

        assertTrue(generator.hasNext());

        Population neighbors = new Population();
        Solution neighbor;
        while(generator.hasNext()) {
            neighbor = generator.nextNeighbor();
            neighbors.add(neighbor);
            assertNotEquals(neighbor.freeRulesString(), solution.freeRulesString());
        }

        Set<Solution> set = new HashSet<>(neighbors);
        neighbors.clear();
        neighbors.addAll(set);

        for(int i=0; i<neighbors.size(); i++) {
            for(int j=i+1; j<neighbors.size(); j++) {
                assertNotEquals("i:" + i + " j:" + j + "\n" + neighbors.get(i) + "\n" + neighbors.get(j),
                        neighbors.get(i),
                        neighbors.get(j)
                );
                assertNotSame(neighbors.get(i), neighbors.get(j));
            }
        }
        assertEquals(85*3, neighbors.size());
    }
}
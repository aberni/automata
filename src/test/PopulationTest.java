package test;

import automata.Initialization;
import automata.Population;
import automata.Solution;
import org.junit.Test;
import static org.junit.Assert.*;

public class PopulationTest {
    @Test
    public void constructor() {
        Population pop = new Population(
                new Solution(Initialization.nbRules),
                new Solution(Initialization.nbRules)
        );

        assertEquals(2, pop.size());
        assertNotNull(pop.get(0));
        assertNotNull(pop.get(1));
    }
}
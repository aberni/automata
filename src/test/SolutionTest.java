package test;

import automata.Initialization;
import automata.Solution;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {
    private static Solution solution;

    @Before
    public void setUp() throws Exception {
        solution = new Solution(Initialization.nbRules);
    }

    @Test
    public void copySolution() {
        Solution copy = new Solution(Initialization.nbRules);

        assertNotSame(copy.getRules(), solution.getRules());

        // default rules and fitness
        solution.setRuleAt(5, 2);
        solution.setRuleAt(0,6);
        solution.setFitness(4);

        int[] expectedRules = new int[Initialization.nbRules];
        assertArrayEquals(expectedRules, copy.getRules());
        assertEquals(0, copy.getFitness());

        // same rules
        copy.copySolution(solution);
        assertArrayEquals(solution.getRules(), copy.getRules());
    }

    @Test
    public void rules() {
        int[] expectedRules = new int[Initialization.nbRules];
        assertArrayEquals(expectedRules, solution.getRules());
        assertEquals(0, solution.getRuleAt(0));

        // modif des règles
        solution.setRuleAt(0, 4);
        assertEquals(4, solution.getRuleAt(0));
        expectedRules[0] = 4;
        assertArrayEquals(expectedRules, solution.getRules());
    }

    @Test
    public void getterSetterFitness() {
        assertEquals(0, solution.getFitness());
        solution.setFitness(3);
        assertEquals(3, solution.getFitness());
    }
}